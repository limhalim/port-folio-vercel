import React from "react";
import { Container, Row, Col } from "reactstrap";
import Link from "next/link";
import SectionSubtitle from "./SectionSubtitle";
import classes from "../../styles/contact.module.css";
import Form from "./Form";

const Contact = () => {
  return (
    <section id="contact" className={`${classes.contact}`}>
      <Container>
        <Row>
          <Col lg="6" md="6">
            <SectionSubtitle subtitle="Contact me" />
            <h3 className="mt-4 mb-4">Contact with me</h3>
            <p>
              Contact me if you have questions or want to communicate further,
              you can click on my social media below.
            </p>

            <ul className={`${classes.contact__info__list}`}>
              <li className={`${classes.info__item}`}>
                <span>
                  <i className="ri-map-pin-line"></i>
                </span>
                <p>Jakarta - Indonesia</p>
              </li>
              <li className={`${classes.info__item}`}>
                <span>
                  <i className="ri-mail-line"></i>
                </span>
                <p>limhalimop@gmail.com</p>
              </li>
              <li className={`${classes.info__item}`}>
                <span>
                  <i className="ri-linkedin-line"></i>
                </span>
                <p>halimlim</p>
              </li>
            </ul>

            <div className={`${classes.social__links}`}>
              <Link href="github.com/limpixel">
                <i className="ri-github-line"></i>
              </Link>

              <Link href="https://dribbble.com/IOK">
                <i className="ri-dribbble-line"></i>
              </Link>
              <Link href="https://www.linkedin.com/in/halimlim/">
                <i className="ri-linkedin-line"></i>
              </Link>
              <Link href="https://www.instagram.com/nep.lims/">
                <i className="ri-instagram-line"></i>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Contact;
