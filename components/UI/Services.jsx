import React from "react";
import { Container, Row, Col } from "reactstrap";
import SectionSubtitle from "./SectionSubtitle";
import classes from "../../styles/services.module.css";
import ServicesItem from "./ServicesItem";

const Services = () => {
  return (
    <section id="services">
      <Container>
        <Row>
          <Col lg="6" md="6">
            <div className={`${classes.services__container}`}>
              <div>
                <ServicesItem
                  title="App Development"
                  icon="ri-database-2-line"
                />

                <ServicesItem title="UI/UX Design" icon="ri-drag-drop-line" />
              </div>

              <ServicesItem
                title="Front-end Developer"
                icon="ri-code-s-slash-line"
              />
            </div>
          </Col>

          <Col lg="6" md="6" className={`${classes.service__title}`}>
            <SectionSubtitle subtitle="What I do" />
            <h3 className="mb-0 mt-4">Better Design,</h3>
            <h3 className="mb-4">Better Experience</h3>
            <p>
              I am a Designer proficient in Figma applications, Whimsical and
              web applications. I create mobile app designs or website designs
              using Figma as my software for creating UI Designs and I create UX
              on Whimscial websites. I am also a Front-end developer proficient
              in React js and Next js framework with tailwindcss for css style I
              can build websites
            </p>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Services;
