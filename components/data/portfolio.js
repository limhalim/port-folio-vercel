const portfolio = [
  {
    id: "01",
    title: "Study App Design",
    img: "/images/portfolio-01.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16840464-Study-App-Design",
  },

  {
    id: "02",
    title: "UI App Qur'an",
    img: "/images/portfolio-02.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16972260-UI-App-Qur-an",
  },

  {
    id: "03",
    title: "IMovie  UI App",
    img: "/images/portfolio-03.jpeg",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl:
      "https://dribbble.com/shots/15631481-IMovie-create-your-be-fun-and-happy",
  },

  {
    id: "04",
    title: "UI Recipe App",
    img: "/images/project-img-13.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16753710-UI-Recipe-App",
  },

  {
    id: "05",
    title: "Food App",
    img: "/images/portfolio-05.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16796200-Food-App",
  },

  {
    id: "06",
    title: "UI Design News App",
    img: "/images/portfolio-06.png",
    category: "Mobile App",
    keyword: ["Mobile", "App", "UI-UX"],
    liveUrl: "https://dribbble.com/shots/16703949-UI-Design-News-App",
  },

  {
    id: "07",
    title: "Portfolio Website",
    img: "/images/portfolio-07.png",
    category: "Web Design",
    keyword: ["Web", "Web design", "UI-UX"],
    liveUrl: "#",
  },

  {
    id: "08",
    title: "Restaurant Website",
    img: "/images/portfolio-08.png",
    category: "Web Design",
    keyword: ["Web", "Web design", "UI-UX"],
    liveUrl: "#",
  },

  {
    id: "09",
    title: "Agency Website",
    img: "/images/portfolio-09.png",
    category: "Web Design",
    keyword: ["Web", "Web design", "UI-UX"],
    liveUrl: "#",
  },
];

export default portfolio;
